﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FacebookAuth.Startup))]
namespace FacebookAuth
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
